import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditComponent } from './components/edit/edit.component';
import {UsersEditRoutingModule} from "./users-edit-routing.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";
import {TranslateModule} from "@ngx-translate/core";



@NgModule({
  declarations: [
    EditComponent
  ],
    imports: [
        CommonModule,
        UsersEditRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        TranslateModule
    ]
})
export class UsersEditModule { }
