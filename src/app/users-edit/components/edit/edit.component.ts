import {Component, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UsersService} from "../../services/users.service";
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../../interfaces/user";
import {FormValidators} from "../../../shared/classes/FormValidators";
import {BsModalRef, BsModalService} from "ngx-bootstrap/modal";
import {AuthService} from "../../../auth/services/auth.service";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  action: 'add' | 'edit' | 'view' | undefined;
  id: number | undefined;
  form: FormGroup = new FormGroup({});
  modalRef?: BsModalRef;

  get uniFormGroup (): FormArray {
    return this.form.get('uni') as FormArray
  }

  get isView(): boolean {
    return this.action === 'view';
  }

  constructor(
    private route: ActivatedRoute,
    private usersService: UsersService,
    private router: Router,
    private userService: UsersService,
    private modalService: BsModalService,
    public authService: AuthService
  ) { }

  ngOnInit(): void {
    const snapshot = this.route.snapshot;
    this.action = snapshot.params.action || 'add';
    this.id = +snapshot.params.id || 0;

    this.form = new FormGroup({
      id: new FormControl(null),
      firstName: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100),
        FormValidators.alpha
      ]),
      lastName: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100),
        FormValidators.alpha
      ]),
      photo: new FormControl(''),
      online: new FormControl(''),
      uni: new FormArray([])
    });
    if (this.action === 'add') {
      this.addNewUni();
    } else {
      const user: User = snapshot.data.user;
      user.uni.forEach(uni => {
        this.addNewUni();
      });
      this.form.patchValue(user);
      if (this.action === 'view') {
        this.form.disable();
      }
    }
    if(this.action === 'view') {
      this.form.disable();
    }
  }

  createUniForm(): FormGroup {
    return new FormGroup({
      title: new FormControl('', [
        Validators.required,
        FormValidators.alphaNumericSpaces
      ]),
      speciality: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100),
        FormValidators.alphaNumericSpaces
      ]),
      start: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(100),
        FormValidators.numeric
      ]),
      end: new FormControl('', [
        Validators.required,
        FormValidators.numeric
      ]),
    }, [
      FormValidators.uniPeriodValidator
    ]);
  }

  submit(): void {
    const v = this.form.value;
    switch (this.action) {
      case 'add':
        this.usersService.create(this.form.value).subscribe(v => {
          this.router.navigate(['/']);
        });
        break;
      case 'edit':
        this.usersService.edit(v).subscribe(v =>{
          this.router.navigate(['/']);
        });
        break;
    }
  }

  addNewUni(): void {
    (this.form.get('uni') as FormArray).push(this.createUniForm());
  }

  removeUni(index: number): void {
    (this.form.get('uni') as FormArray).removeAt(index);
  }

  uploadFile(inputValue: any): void {
    let file:File = inputValue.files[0];
    let myReader:FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.form.get('photo')?.setValue(myReader.result);
    }
    myReader.readAsDataURL(file);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
  }

  delete(): void {
    this.userService.delete(this.form.get('id')?.value).subscribe(v => {
      this.router.navigate(['/']);
    });
  }

}
