import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {EditComponent} from "./components/edit/edit.component";
import {UserResolver} from "./resolvers/user.resolver";
import {IsAdminGuard} from "../shared/guards/is-admin.guard";

const routes: Routes = [
  {
    path: ':action/:id',
    component: EditComponent,
    canActivate: [IsAdminGuard],
    resolve: {
      user: UserResolver
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UsersEditRoutingModule { }
