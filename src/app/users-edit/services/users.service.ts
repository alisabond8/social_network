import {Injectable} from '@angular/core';
import {HttpService} from "../../shared/services/http.service";
import {Observable} from "rxjs";
import {Roles, User} from "../../interfaces/user";
import {switchMap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(
    private http: HttpService
  ) { }

  getUsers(): Observable<User[]> {
    return this.http.get('users');
  }

  getUserById(id: number): Observable<User> {
    return this.http.get(`users/${id}`);
  }

  create(user: User): Observable<void> {
    user.online = false;
    user.role = user.role || Roles.USER;

    return this.getUsers().pipe(
      switchMap((users) => {
        // SETTING THIS BECAUSE OF LocalInMemoryDbService BUG
        // IT CAN NOT AUTOMATICALLY GENERATE ID FOR ENTITY IF DB IS EMPTY
        if(!users.length) {
          user.id = 1;
        }
        return this.http.post<void>('users', user);
      })
    )
  }

  edit(aOwner: User): Observable<User> {
    return this.http.put(`users`, aOwner);
  };

  delete(id: number): Observable<User[]> {
    return this.http.delete(`users/${id}`);
  };
}
