import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import {UsersService} from "../services/users.service";
import {User} from "../../interfaces/user";

@Injectable({
  providedIn: 'root'
})
export class UserResolver implements Resolve<User | boolean> {

  constructor(
    private usersService: UsersService
  ) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User | boolean> {
    const action = route.params.action || 'add';
    const id = +route.params.id || 0;
    if (action !== 'add') {
      return this.usersService.getUserById(id);
    }
    return of(true);
  }
}
