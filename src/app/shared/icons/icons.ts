import {list, xLg, trash, plusCircle, eye, pencilSquare, arrowLeftCircle, download} from "ngx-bootstrap-icons";

export const icons = {
  list,
  xLg,
  trash,
  plusCircle,
  eye,
  pencilSquare,
  arrowLeftCircle,
  download
}
