import {Directive, ElementRef, Input, OnChanges, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[appOnline]'
})
export class OnlineDirective implements OnChanges {
  @Input() appOnline: boolean | undefined;
  constructor(
    private el: ElementRef
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.appOnline) {
      const v = changes.appOnline?.currentValue;
      this.el.nativeElement.style.backgroundColor = v ? '#619C28' : '#ea3232';
    }
  }

}
