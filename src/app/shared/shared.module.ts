import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import {NgxBootstrapIconsModule} from "ngx-bootstrap-icons";
import {icons} from "./icons/icons";
import {TranslateModule} from "@ngx-translate/core";
import { ErrorMessagePipe } from './pipes/error-message.pipe';
import { FormErrorComponent } from './components/form-error/form-error.component';
import { ShortFullNamePipe } from './pipes/short-full-name.pipe';
import { OnlineDirective } from './directives/online.directive';
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    ErrorMessagePipe,
    FormErrorComponent,
    ShortFullNamePipe,
    OnlineDirective
  ],
    imports: [
        CommonModule,
        NgxBootstrapIconsModule.pick(icons),
        TranslateModule,
        RouterModule,
    ],
  exports: [
    HeaderComponent,
    FooterComponent,
    NgxBootstrapIconsModule,
    ErrorMessagePipe,
    FormErrorComponent,
    ShortFullNamePipe,
    OnlineDirective
  ]
})
export class SharedModule { }
