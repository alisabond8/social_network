import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shortFullName'
})
export class ShortFullNamePipe implements PipeTransform {

  transform(firstName: string, lastName: string): string {
    firstName = firstName || '';
    lastName = lastName || '';
    return `${firstName} ${lastName.substr(0, 1)}.`;
  }

}
