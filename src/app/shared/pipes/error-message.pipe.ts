import { Pipe, PipeTransform } from '@angular/core';
import {AbstractControl} from "@angular/forms";

@Pipe({
  name: 'errorMessage',
  pure: false
})
export class ErrorMessagePipe implements PipeTransform {

  transform(control: AbstractControl | null | undefined): string[] {
    if (!control) {
      return [];
    }
    if (!control.errors) {
      return [];
    }
    return Object.keys(control.errors);
  }

}
