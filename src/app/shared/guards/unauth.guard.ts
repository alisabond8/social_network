import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import {CanActivate, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from "../../auth/services/auth.service";

@Injectable({
  providedIn: 'root'
})
export class UnauthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private location: Location
  ) {
  }

  canActivate(): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.authService.isLoggedIn) {
      this.location.back();
    }
    return !this.authService.isLoggedIn;
  }


}
