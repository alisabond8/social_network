import {AbstractControl, FormControl, FormGroup} from "@angular/forms";

export class FormValidators {

  static email(ctrl: FormControl): { [key: string]: boolean } | null {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const valid = (ctrl.value + '').match(regex);
    return valid ? null : { email: true };
  }

  static uniPeriodValidator(ctrl: AbstractControl): { [key: string]: boolean } | null {
    const v = ctrl.value;
    if (!v.start || !v.end) {
      return null;
    }
    if (!+v.start || !+v.end || +v.start >= +v.end) {
      return {
        uniPeriod: true
      }
    }
    return null;
  }

  static alpha(ctrl: FormControl): { [key: string]: boolean } | null {
    const regex = /^[\-A-Za-zа-яА-Я]*$/;
    const valid = (ctrl.value + '').match(regex);
    return valid ? null : { alpha: true };
  }

  static alphaNumericSpaces(ctrl: FormControl): { [key: string]: boolean } | null {
    const regex = /^[\-A-Za-zа-яА-Я0-9 ]*$/;
    const valid = (ctrl.value + '').match(regex);
    return valid ? null : { alphaNumeric: true };
  }

  static alphaNumeric(ctrl: FormControl): { [key: string]: boolean } | null {
    const regex = /^[\-A-Za-zа-яА-Я0-9]*$/;
    const valid = (ctrl.value + '').match(regex);
    return valid ? null : { alphaNumeric: true };
  }

  static numeric(ctrl: FormControl): { [key: string]: boolean } | null {
    const regex = /^[0-9]*$/;
    const valid = (ctrl.value + '').match(regex);
    return valid ? null : { numeric: true };
  }

}
