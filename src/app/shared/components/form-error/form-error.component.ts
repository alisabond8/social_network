import {Component, Input, OnInit, SimpleChanges} from '@angular/core';
import {AbstractControl} from "@angular/forms";

@Component({
  selector: 'app-form-error',
  templateUrl: './form-error.component.html',
  styleUrls: ['./form-error.component.scss']
})
export class FormErrorComponent implements OnInit {
  @Input() control: AbstractControl | undefined | null;

  constructor() { }

  ngOnInit(): void {
  }

}
