import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SignInGuard} from "./shared/guards/sign-in.guard";
import {UnauthGuard} from "./shared/guards/unauth.guard";
import {BaseComponent} from "./core/components/base/base.component";

const routes: Routes = [
  {
    canActivate: [UnauthGuard],
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    canActivate: [SignInGuard],
    path: '',
    component: BaseComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./users-list/users-list.module').then(m => m.UsersListModule)
      },
      {
        path: 'users',
        loadChildren: () => import('./users-edit/users-edit.module').then(m => m.UsersEditModule)
      },
    ]
  },
  {
    path: '**',
    redirectTo: 'auth'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
