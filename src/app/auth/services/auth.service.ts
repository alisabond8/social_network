import { Injectable } from '@angular/core';
import {HttpService} from "../../shared/services/http.service";
import {Observable, throwError} from "rxjs";
import {map} from "rxjs/operators";
import {Roles, User} from "../../interfaces/user";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private currentUser: User | undefined;
  private readonly users = [
    {
      login: 'asd@asd.asd',
      password: '00000000',
      id: 1
    },
    {
      login: 'qwe@qwe.qwe',
      password: '11111111',
      id: 2
    }
  ];

  get isLoggedIn() {
    return !!this.currentUser;
  }

  get isAdmin() {
    return this.currentUser?.role == Roles.ADMIN;
  }

  constructor(
    private http: HttpService,
    private router: Router
  ) { }

  login(value: {login: string, password: string}): Observable<User> {
    const user = this.users.filter(user => {
      return user.login == value.login && user.password == value.password;
    }).shift();
    if (user) {
      return this.http.get<User>(`users/${user.id}`).pipe(
        map((user) => {
          this.currentUser = user;
          return user;
        })
      );
    }
    return throwError(new Error('User not found'));
  }

  logout() {
    this.currentUser = undefined;
    this.router.navigate(['/auth']);
  }
}
