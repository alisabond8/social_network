import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {FormValidators} from "../../../shared/classes/FormValidators";
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {TranslateService} from "@ngx-translate/core";
import {take} from "rxjs/operators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup = new FormGroup({
    login: new FormControl('', [
      Validators.required,
      Validators.minLength(4),
      FormValidators.email,
      Validators.maxLength(25)
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      Validators.maxLength(25)
    ])
  });

  constructor(
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService,
    private translate: TranslateService
  ) {}

  ngOnInit(): void {}

  login(event: MouseEvent): void {
    event.preventDefault();
    if (!this.form.valid) {
      return;
    }
    this.authService.login(this.form.value).subscribe(v => {
      this.router.navigate(['']);
      // this.toastr.success('You successfully logged in');
    }, err => {
      this.translate.get(['login.invalid_credentials', 'login.error']).pipe(take(1)).subscribe((res) => {
        this.toastr.error(res['login.invalid_credentials'], res['login.error']);
      });
    });
  }

}
