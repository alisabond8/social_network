import { Component } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router, RouterEvent} from "@angular/router";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  loading: boolean = false;
  constructor(
    private translate: TranslateService,
    private router: Router
  ) {
    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them

    if (localStorage.getItem('lang')) {
      this.translate.use(localStorage.getItem('lang') || 'en');
    } else {
      this.translate.use('en');
    }

    router.events.pipe(
      filter((e: unknown) => e instanceof RouterEvent)
    ).subscribe((e: unknown) => {
      this.navigationInterceptor(e as RouterEvent);
    })
  }

  // Shows and hides the loading spinner during RouterEvent changes
  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.loading = true
    }

    if(event instanceof NavigationEnd
    || event instanceof NavigationCancel
    || event instanceof NavigationError) {
      this.loading = false;
    }
  }
}
