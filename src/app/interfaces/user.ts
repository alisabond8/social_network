export enum Roles {
  USER = 'user',
  ADMIN = 'admin'
}

export interface University {
  title: string;
  speciality: string;
  start: number;
  end: number;
}

export interface User {
  role: Roles;
  id: number;
  firstName: string;
  lastName: string;
  photo: string;
  online: boolean;
  uni: University[];
}
