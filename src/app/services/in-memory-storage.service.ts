import {Injectable} from '@angular/core';
import {InMemoryDbService} from "angular-in-memory-web-api";
import {Roles, User} from "../interfaces/user";

@Injectable({
  providedIn: 'root'
})
export class InMemoryStorageService implements InMemoryDbService {
  createDb() {
    let users: User[] = [
      {
        role: Roles.ADMIN,
        id: 1,
        firstName: 'Ivan',
        lastName: 'Ivanov',
        photo: '',
        online: true,
        uni: [
          {
            end: 1991,
            start: 1990,
            speciality: "Speciality 1",
            title: "University 1"
          },
          {
            end: 1993,
            start: 1992,
            speciality: "Speciality 2",
            title: "University 2"
          }
        ]
      },
      {
        role: Roles.USER,
        id: 2,
        firstName: 'Petr',
        lastName: 'Petrov',
        photo: '',
        online: false,
        uni: [
          {
            end: 1995,
            start: 1994,
            speciality: "Speciality 3",
            title: "University 3"
          }
        ]
      }
    ]
    return { users };
  }

  constructor() { }
}
