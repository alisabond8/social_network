import { Component, OnInit } from '@angular/core';
import {User} from "../../../interfaces/user";
import {ActivatedRoute} from "@angular/router";
import {UsersListService} from "../../services/users-list.service";
import {AuthService} from "../../../auth/services/auth.service";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  users: User[] | undefined;
  foundUsers: User[] | undefined;
  constructor(
    private route: ActivatedRoute,
    private usersListService: UsersListService,
    public authService: AuthService
  ) { }

  ngOnInit(): void {

    this.users = this.route.snapshot.data?.users;
  }

  preventIfNotSymbol(text: string, event: Event) {
    const isSymbol = /^[a-zA-Zа-яА-Я- 0-9]$/.test(text);
    if (!isSymbol) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  onKeyUp(event: KeyboardEvent) {;
    const key = event.key;
    if(event.key === "Backspace" || event.key === 'Delete') {
      return;
    }
    this.preventIfNotSymbol(key, event);
  }

  test(event: ClipboardEvent) {
    let paste = (event.clipboardData || (window as any).clipboardData).getData('text');
    this.preventIfNotSymbol(paste, event);
  }

  onSearch(event: KeyboardEvent): void {
    const target = (event.target as HTMLInputElement);
    if (!target.value) {
      this.foundUsers = [];
    } else {
      this.foundUsers = this.usersListService.search(this.users || [], target.value);
    }
  }

}
