import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import { ListComponent } from "./components/list/list.component";
import {UserListResolver} from "./resolvers/user-list.resolver";

const routes: Routes = [
  {
    path: '',
    component: ListComponent,
    resolve: {
      users: UserListResolver
    }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UsersListRoutingModule { }
