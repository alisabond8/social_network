import { Injectable } from '@angular/core';
import {User} from "../../interfaces/user";

@Injectable({
  providedIn: 'root'
})
export class UsersListService {

  constructor() { }

  search(users: User[], searchStr: string): User[] {
    return users.filter(user => {
      const s = searchStr.toLowerCase();
      const fields = ['lastName', 'firstName'];
      return fields.some((field: string) => (user as any)[field].toLowerCase().indexOf(s) > -1);
    })
  }
}
