import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './components/list/list.component';
import {UsersListRoutingModule} from "./users-list-routing.module";
import {TranslateModule} from "@ngx-translate/core";
import {SharedModule} from "../shared/shared.module";



@NgModule({
  declarations: [
    ListComponent
  ],
    imports: [
        CommonModule,
        UsersListRoutingModule,
        TranslateModule,
        SharedModule
    ]
})
export class UsersListModule { }
